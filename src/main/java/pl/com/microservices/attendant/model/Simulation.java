package eu.microwebservices.simulation.model;

import lombok.AccessLevel;
import lombok.Setter;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

// import org.springframework.data.elasticsearch.annotations.Document;


@Data
//@Document(indexName = "simulationdb", type = "simulation")
@Entity
@Table(name = "simulation")
public class Simulation {

   /**
    * 
// https://projectlombok.org/features/Data
// https://stormit.pl/lombok/#data
// https://medium.com/@ashish_fagna/getting-started-with-elasticsearch-creating-indices-inserting-values-and-retrieving-data-e3122e9b12c6
// https://stackoverflow.com/questions/59278195/spel-used-in-document-indexname-with-spring-data-elasticsearch-and-spring-boot
    */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @Setter(AccessLevel.PACKAGE)
    private String name;

    @Setter(AccessLevel.PACKAGE)
    private Double score;

    @Setter(AccessLevel.PACKAGE)
    private Long userid;

    @Setter(AccessLevel.PACKAGE)
    private Long visitorid;

    @Setter(AccessLevel.PACKAGE)
    private Long worldid;

//    public Simulation() {
//    }
//
    public Simulation(Long id) {
        this.id = id;
    }

    public Simulation(String name, Double score, Long userid, Long visitorid, Long worldid) {
        this.name = name;
        this.score = score;
        this.userid = userid;
        this.visitorid = visitorid;
        this.worldid = worldid;
    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
    public void setName(String name) {
        this.name = name;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public void setVisitorid(Long visitorid) {
        this.visitorid = visitorid;
    }

    public void setWorldid(Long worldid) {
        this.worldid = worldid;
    }

//
//    @Override
//    public String toString() {
//        return "Simulation{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                ", price=" + price +
//                '}';
//    }
    
} 
