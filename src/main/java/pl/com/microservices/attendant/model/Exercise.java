package eu.microwebservices.simulation.model;

import lombok.AccessLevel;
import lombok.Setter;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

// import org.springframework.data.elasticsearch.annotations.Document;


//@Document(indexName = "simulationdb", type = "exercise")
    @Entity
    @Table(name = "exercise")
    // @ToString(includeFieldNames=true)
    @Data
    // @Data(staticConstructor="of")
    public class Exercise /* <T> */ {

   /**
    * 
// https://projectlombok.org/features/Data
// https://stormit.pl/lombok/#data
// https://medium.com/@ashish_fagna/getting-started-with-elasticsearch-creating-indices-inserting-values-and-retrieving-data-e3122e9b12c6
// https://stackoverflow.com/questions/59278195/spel-used-in-document-indexname-with-spring-data-elasticsearch-and-spring-boot
    */

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @Setter(AccessLevel.PACKAGE)
        private Long simulationid;

        @Setter(AccessLevel.PACKAGE)
        private Long attendantid;

 	/* 
	@Setter(AccessLevel.PACKAGE)
        private T value;  */

    public Exercise(Long id) {
        this.id = id;
    }

    public Exercise(Long simulationid, Long attendantid) {
        this.simulationid = simulationid;
        this.attendantid = attendantid;
    }

    public void setSimulationid(Long simulationid) {
        this.simulationid = simulationid;
    }

    public void setAttendantid(Long attendantid) {
        this.attendantid = attendantid;
    }

} 
