package pl.com.microservices.attendant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class AttendantApplication {

	public static void main(String[] args) {
		SpringApplication.run(AttendantApplication.class, args);
	}

	@GetMapping("/hallo")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hallo %s! Attendant app is alive!", name);
	}

}
