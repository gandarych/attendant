package eu.microwebservices.simulation.controller;

import java.math.BigInteger;

// import java.util.Arrays;
// import java.util.stream.Stream;
import java.util.Optional;
import java.util.logging.Logger;

import eu.microwebservices.simulation.model.Simulation;
import eu.microwebservices.simulation.model.SimulationDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.dao.*;
import org.springframework.data.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
// import org.springframework.web.servlet.tags.Param;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@Controller
public class SimulationController {
   /**
    * HOW TO TEST:
    * $ mvn spring-boot:run
    * http://localhost:8087/
    * Use the following urls:
    *    /simulation/create?name=[name]&score=[score]:
    *          create a new simulation with an auto-generated id and price and name as passed values.
    *    /simulation/delete?id=[id]:
    *          delete the simulation with the passed id.
    *    /simulation/update?id=[id]&email=[email]&name=[name]:
    *          update the price and the name for the simulation indentified by the passed id.
    */

  @Autowired
  private SimulationDao simulationDao;

	@GetMapping(value = "/simulation/{id}")
	public Simulation getSimulation(@PathVariable("id") /* BigInteger */ Long id) {
        Simulation simulation = null;
        try {
            Optional<Simulation> SimulationOptional =
                    Optional.ofNullable(simulationDao.findById(BigInteger.valueOf(id)));
            if (SimulationOptional.isPresent()) {
                simulation = SimulationOptional.get();;
            }
        } catch (Exception ex) {
            // @TODO @FIXME
        }
	    return simulation;
	}
  
  /**
   * /create-simulation  --> Create a new simulation and save it in the database.
   * It is not secure operation here! There is no validation here!  See https://www.owasp.org
   * It is only for REST educational purposes...
   * 
   * @param name Simulation's name
   * @param score Simulation's score
   * @return A string describing if the simulation is successfully created or not.
   */
  @RequestMapping("/simulation/create")
  @ResponseBody
  public String create(String name, Double score, Long userid, Long visitorid, Long worldid) {
    Simulation simulation = null;
    try {
      simulation = new Simulation(name, score, userid, visitorid, worldid);
      simulationDao.save(simulation);
    }
    catch (Exception ex) {
      return "Error while creating the simulation: " + ex.toString();
    }
    return "User created succesfully!! (id = " + simulation.getId() + ")";
  }
  
  /**
   * /delete-simulation  --> Delete the simulation having the passed id.
   * It is not secure operation here! There is no validation here!
   * It is only for REST educational purposes...
   * 
   * @param id The id of the simulation to delete
   * @return A string describing if the simulation is successfully deleted or not.
   */
  @RequestMapping("/simulation/delete")
  @ResponseBody
  public String delete(Long id) {
    try {
      Simulation simulation = new Simulation(id);
      simulationDao.delete(simulation);
    }
    catch (Exception ex) {
      return "Error while deleting the simulation: " + ex.toString();
    }
    return "Simulation deleted successfully!!";
  }
  
  /**
   * /update-simulation  --> Update the price and the name for the simulation in the database 
   * having the passed id.
   * It is not secure operation here! There is no validation here!
   * It is only for REST educational purposes...
   * 
   * @param id The id for the simulation to update.
   * @param name The new name.
   * @param score The new score.
   * @return A string describing if the simulation is successfully updated or not.
   */
  @RequestMapping("/simulation/update")
  @ResponseBody
  public String updateSimulation(Long id, String name, Double score, Long userid, Long visitorid, Long worldid) {
    Simulation simulation = null;
    try {
		Optional<Simulation> SimulationOptional =
                Optional.ofNullable(simulationDao.findById(BigInteger.valueOf(id)));
		if (SimulationOptional.isPresent()) {
		      simulation = SimulationOptional.get();
		      simulation.setName(name);
		      simulation.setScore(score);
		      simulation.setUserid(userid);
		      simulation.setVisitorid(visitorid);
		      simulation.setWorldid(worldid);
		      simulationDao.save(simulation);
	    }
    } catch (Exception ex) {
      return "Error while updating the simulation: " + ex.toString();
    }
    return "Simulation updated successfully!!";
  }
  
}
