CREATE TABLE attendant (
	id INT PRIMARY KEY AUTO_INCREMENT,
    	name VARCHAR(255), 
	score DOUBLE,
	userid INT,
	visitorid INT,
	worldid INT
);

INSERT INTO attendant (id, name, score, userid, visitorid, worldid) VALUES (1, 'Good game 1', 20.0, 1, 1, 1); 
INSERT INTO attendant (id, name, score, userid, visitorid, worldid) VALUES (2, 'Testing castle 1', 5.0, 1, 2, 1); 
INSERT INTO attendant (id, name, score, userid, visitorid, worldid) VALUES (3, 'TOC', 10.0, 1, 3, 2); 
INSERT INTO attendant (id, name, score, userid, visitorid, worldid) VALUES (4, 'etc', 35.0, 1, 2, 1); 
INSERT INTO attendant (id, name, score, userid, visitorid, worldid) VALUES (5, 'Good game 2', 80.0, 1, 1, 1); 
INSERT INTO attendant (id, name, score, userid, visitorid, worldid) VALUES (6, 'My simula 1', 15.0, 1, 2, 2); 
INSERT INTO attendant (id, name, score, userid, visitorid, worldid) VALUES (7, 'My simula 1', 30.0, 1, 3, 1); 
INSERT INTO attendant (id, name, score, userid, visitorid, worldid) VALUES (8, 'My simula 1', 75.0, 1, 1, 2); 

