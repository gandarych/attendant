#!/bin/bash

###### 06_01_install_terraform_AS_ROOT on ubuntu WHEN WE WORK AS ROOT
echo "RUNNING 06_01_install_terraform_AS_ROOT.sh"
# docker run -it ubuntu bash

# Your command prompt should change to reflect the fact that you're now working inside the container:

## OUTPUT:
#kris@gandalf1:~$     docker run -it ubuntu bash
#root@dfffe6a3e74d:/#

# Pay attention! Note the container ID in the command prompt: it is dfffe6a3e74d.
# You'll need that container ID later to identify the container when you want to remove it.


# Now you can run any command inside the container:
echo "apt update"
apt update

# Any changes you make inside the container only apply to that container.
# To exit the container, type "exit" at the prompt.
echo "snap install terraform"
snap install terraform
## apt install -y terraform
## apt install pip
## pip install terraform
echo "terraform --version"
terraform --version
echo "apt install -y net-tools"
apt install -y net-tools
echo "ifconfig"
ifconfig
echo "apt install -y ping"
apt install -y ping
##or## apt-get install -y ping
echo "apt install -y iputils-ping"
apt install -y iputils-ping
##or## apt install -y inetutils-ping
echo "apt install -y telnet"
apt install -y telnet
##or## apt-get install -y telnet

##  ping 192.168.1.22
## telnet 192.168.1.22 22
## telnet 192.168.1.22 13531 
echo "terraform --help"
terraform --help

####   https://github.com/startup-systems/terraform-ansible-example
####   https://www.hashicorp.com/resources/ansible-terraform-better-together
####   https://alex.dzyoba.com/blog/terraform-ansible/
####   https://medium.com/faun/building-repeatable-infrastructure-with-terraform-and-ansible-on-aws-3f082cd398ad
####   https://victorops.com/blog/writing-ansible-playbooks-for-new-terraform-servers
####   https://getintodevops.com/blog/using-ansible-with-terraform
####   https://www.redhat.com/cms/managed-files/pa-terraform-and-ansible-overview-f14774wg-201811-en.pdf
####   https://stackshare.io/stackups/ansible-vs-terraform    (LOGO, following)

####   Packer provisioner runs Ansible playbooks!!!:
####   https://packer.io/docs/provisioners/ansible.html
