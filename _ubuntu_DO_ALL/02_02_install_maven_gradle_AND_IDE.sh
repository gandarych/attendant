#!/bin/bash

# 02_02_install_maven_AND_IDE on ubuntu
bash pwd
echo "RUNNING 02_02_install_maven_AND_IDE.sh"
# remove old if exists:
echo "cd /opt/"
cd /opt/
echo "sudo rm -r /opt/maven-3.6.3/"
sudo rm -r /opt/maven-3.6.3/
# download the latest stable version of Apache Maven from the official website:
echo "sudo wget https://www-us.apache.org/dist/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz"
sudo wget https://www-us.apache.org/dist/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
# extract the downloaded archive:
echo "sudo tar -xvzf apache-maven-3.6.3-bin.tar.gz"
sudo tar -xvzf apache-maven-3.6.3-bin.tar.gz
# rename the extracted directory:
echo "sudo mkdir /opt/maven/"
sudo mkdir /opt/maven/
echo "sudo mv /opt/apache-maven-3.6.3 /opt/maven/maven-3.6.3"
sudo mv /opt/apache-maven-3.6.3 /opt/maven/maven-3.6.3
sudo cp -pr /tmp/maven_conf/settings.xml /opt/maven/maven-3.6.3/conf
echo "cd /opt/maven/maven-3.6.3/conf"
cd /opt/maven/maven-3.6.3/conf
sudo ls -al

echo "sudo rm /opt/apache-maven-3.6.3-bin.tar.gz"
sudo rm /opt/apache-maven-3.6.3-bin.tar.gz

echo "sudo apt install -y gradle"
sudo apt install -y gradle
gradle -v

echo "/usr/bin/gradle -v"
/usr/bin/gradle -v
##or##

echo "sudo apt install -y wget unzip"
sudo apt install -y wget unzip

# latest version from https://services.gradle.org/distributions/*
cd /tmp/
wget https://services.gradle.org/distributions/gradle-6.3-bin.zip

unzip gradle-*.zip

sudo rm -r /opt/gradle
sudo mkdir /opt/gradle

sudo cp -pr /tmp/gradle-*/* /opt/gradle
sudo rm -r /tmp/gradle-*
sudo rm -r /tmp/gradle-*.zip
sudo ls -al /opt/gradle

#Setting up environment variables

Configure the path environment variable to include the Gradle’s bin directory. 
For this create a new file inside the /etc/profile.d/ directory.

# sudo nano /etc/profile.d/gradle.sh 
export PATH=$PATH:/opt/gradle/bin

# sudo chmod +x /etc/profile.d/gradle.sh
# source /etc/profile.d/gradle.sh

echo "/opt/gradle/bin/gradle -v"
cd /opt/gradle/bin
./gradle -v



echo "sudo apt install -y netbeans"
apt install -y netbeans
# sudo apt install -y netbeans

## Install it from the Software Center [Recommended]   https://itsfoss.com/install-intellij-ubuntu-linux/
## Installing IntelliJ IDEA is available in Ubuntu Software Center
##or##
echo "sudo snap install intellij-idea-community --classic"
snap install intellij-idea-community --classic
# sudo snap install intellij-idea-community --classic

echo "sudo snap install --classic eclipse"
snap install --classic eclipse
# sudo snap install --classic eclipse

## Visual Studio Code
# https://code.visualstudio.com/Download

