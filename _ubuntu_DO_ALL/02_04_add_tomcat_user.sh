#!/bin/bash

## 02_04_add_tomcat_user on ubuntu
bash pwd
echo "RUNNING 02_04_add_tomcat_user.sh"
## https://medium.com/@madeeshafernando/installing-apache-tomcat-on-ubuntu-18-04-8cf8bc63993d
## https://www.liquidweb.com/kb/how-to-install-apache-tomcat-9-on-ubuntu-18-04/

## tomcat user = user will be the one who has group ownership to the Tomcat files
echo "sudo useradd -r -m -U -d /opt/tomcat -s /bin/false tomcat"
sudo useradd -r -m -U -d /opt/tomcat -s /bin/false tomcat
##lub## sudo groupadd tomcat
##lub## sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat

## update permissions to Tomcat - for some of the directories of Tomcat.
echo "sudo chown -RH tomcat: /opt/tomcat/apache-tomcat-9.0.33"
sudo chown -RH tomcat: /opt/tomcat/apache-tomcat-9.0.33
echo "sudo chgrp -R tomcat /opt/tomcat"
sudo chgrp -R tomcat /opt/tomcat
echo "cd /opt/tomcat/apache-tomcat-9.0.33"
cd /opt/tomcat/apache-tomcat-9.0.33
echo "sudo chmod -R g+r conf"
sudo chmod -R g+r conf
echo "sudo chmod g+x conf"
sudo chmod g+x conf
echo "sudo chown -R tomcat webapps/ work/ temp/ logs/"
sudo chown -R tomcat webapps/ work/ temp/ logs/

## Copy the path of Tomcat’s home by running this command:
echo "sudo update-java-alternatives -l"
sudo update-java-alternatives -l
## Output:
## java-1.11.0-openjdk-amd64      1101       /usr/lib/jvm/java-1.11.0-openjdk-amd64</b
## take the highlighted path and put it into your /etc/systemd/system/tomcat.service file, as the JAVA_HOME variable (shown below).
