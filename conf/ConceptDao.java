package eu.microwebservices.simulation.model;

import java.math.BigInteger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import eu.microwebservices.concept.model.Concept;
import java.util.List;

import org.springframework.dao.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
// import org.springframework.web.servlet.tags.Param;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Repository
@Transactional
public interface ConceptDao extends CrudRepository<Concept, Long> {

  /**
   * Find by id.
//    @Query("SELECT boo FROM Simulation boo WHERE boo.fullName=(:pFullName)")
//    Simulation findByFullName(@PathVariable("pFullName") String pFullName);
//  https://www.baeldung.com/spring-data-elasticsearch-tutorial
//  For example, we could search for articles that have the word “data” in the title by 
//  building a query with the NativeSearchQueryBuilder:	
//  SearchQuery searchQuery = new NativeSearchQueryBuilder()
//    .withFilter(regexpQuery("title", ".*data.*"))
//    .build();
//  List<Article> articles = elasticsearchTemplate.queryForList(searchQuery, Article.class);
   */

    @Query("SELECT sim FROM Concept sim WHERE sim.id=(:id)")
    Concept findById(@PathVariable("id") BigInteger  id);

    // void save(Simulation simulation);

    // void delete(Simulation simulation);

/*
wyciaganie roznych typow (jak w switchu):

@FunctionalInterface
interface ThrowingBiFunction<T, N, R, E extends Exception> {
    R apply(T t, N n) throws E;
}

protected <T> T getValueFromResultSet(
			ResultSet resultSet, 
			String valueName, 
			T defaultValue, 
			ThrowingBiFunction<ResultSet, String, T, SQLException> extractor) {

        try {
            return Optional.ofNullable(extractor.apply(resultSet, valueName))
                    .orElse(defaultValue);
        } catch (SQLException e) {
            return defaultValue;
        }
    }

        String abc = getValueFromResultSet(rs, "abc", "te", ResultSet::getString);
        Integer bb123 = getValueFromResultSet(rs, "cc", 321, ResultSet::getInt);
*/
// Solution:
// IntStream.of(A).boxed().collect(Collectors.toList());

}
